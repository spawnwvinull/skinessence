<?php
include "./ProductFactory.php";

echo ("Test 1: 1A en 1B\n");
$productIds = [1, 2];
$productComponents = CreateProductComponents($productIds);
assert(count($productComponents) == 2);
assert($productComponents[0]->count == 1);
assert($productComponents[1]->count == 1);

echo ("Test 2: 2A en 1B\n");
$productIds = [1, 1, 2];
$productComponents = CreateProductComponents($productIds);
assert(count($productComponents) == 2);
assert($productComponents[0]->count == 2);
assert($productComponents[1]->count == 1);

echo ("Test 3: 1A en 2B\n");
$productIds = [1, 2, 2];
$productComponents = CreateProductComponents($productIds);
assert(count($productComponents) == 2);
assert($productComponents[0]->count == 1);
assert($productComponents[1]->count == 2);
