<?php
// find the stock number for this compenent
// divide stock number by the component count
// save this number into a list
function CalculateStock($complexProduct, $productStockList)
{
    $resultList = [];
    foreach ($complexProduct->products as $component) {

        foreach ($productStockList as $item) {
            $product = $item->product;
            if ($component->id == $product->id) {
                $val = $item->numberInStock / $component->count;
                array_push($resultList, $val);
            }
        }
    }

    // return the list minimum 
    return floor(min($resultList));
}
