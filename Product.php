<?php
class Product
{
    public $id;
    public $name;

    function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }
}

class ComplexProductComponent extends Product
{
    public $count;

    function __construct($id, $name, $count)
    {
        $this->count = $count;
        parent::__construct($id, $name);
    }
}
