<?php

function GetProductStockInfo($productIds)
{
    global $wpdb;
    $productStockList = [];
    foreach ($productIds as $id) {
        $select_stock = "SELECT min(meta_value) stock FROM {$wpdb->prefix}postmeta WHERE meta_key = '_stock' AND post_id = $id GROUP BY post_id ORDER BY meta_value";
        $stock = $wpdb->get_results($select_stock);
        $product = new Product($id, "");
        $productStock = new ProductStock($product, $stock);
        array_push($productStockList, $productStock);
    }
    return $productStockList;
}
