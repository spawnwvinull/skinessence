<?php
class ProductStock
{
    public $product;
    public $numberInStock;

    function __construct($product, $numberInStock)
    {
        $this->product = $product;

        if ($numberInStock < 0) {
            $numberInStock = 0;
        }
        $this->numberInStock = $numberInStock;
    }
}
