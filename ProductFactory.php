<?php
include "./Product.php";

function CreateProductComponents($productIds)
{
    $productComponents = [];
    foreach ($productIds as $id) {
        $count = array_count_values($productIds)[$id];
        $product = new ComplexProductComponent($id, "", $count);
        if (!in_array($product, $productComponents)) {
            array_push($productComponents, $product);
        }
    }
    return $productComponents;
}
