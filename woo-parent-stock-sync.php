<?php
include "./GetStockComplexProject.php";
include "./ProductStockFactory.php";
/*
 * Plugin Name: Woo Parent Stock Sync
 * Author: WooExtend
 * Author URI: https://www.wooextend.com
 * Description: This plugin makes stock of parent product to minimum of the child products' stock.
 * Version: 2.0
 * Tested up to: 5.4.2
 * WC tested up to: 3.6 
 */

/**
 * Check if WooCommerce is active
 **/

if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {

	add_action('woocommerce_product_set_stock', 'wooextend_product_set_stock', 10, 1);
	function wooextend_product_set_stock($product)
	{

		// setup the array here (parent => child)
		$arr_setup_product = array(
			55412	=>	array(55411, 55410),
			55398	=>	array(42428, 43137),
			55397	=>	array(42430, 43140),
			55353	=>	array(2214, 2216),
			55357	=>	array(2464, 2465),
			55359	=>	array(2323, 2324),
			55360	=>	array(39012, 39013)
		);

		global $wpdb;

		// Get updated product
		$product_id = $product->get_id();
		foreach ($arr_setup_product as $parent_id => $arr_child) {

			if (in_array($product_id, $arr_child)) {

				$select_stock = "SELECT min(meta_value) stock FROM {$wpdb->prefix}postmeta WHERE meta_key = '_stock' AND post_id IN ('" . implode("','", $arr_child) . "') GROUP BY post_id ORDER BY meta_value";
				$arr_stock_data = $wpdb->get_results($select_stock);

				$arr_stock = array();
				foreach ($arr_stock_data as $key => $value) {

					$arr_stock[] = $value->stock;
				}

				$min_stock = min($arr_stock);

				// new code block
				$productComponents = CreateProductComponents($arr_child);
				$complexProduct = new ComplexProduct($productComponents);
				$productStockList = GetProductStockInfo($productComponents);
				$min_stock = CalculateStock($complexProduct, $productStockList);
				// end new code block


				// create object of parent product
				$parent_product = wc_get_product($parent_id);
				$current_stock = $parent_product->get_stock_quantity();

				$parent_product->set_stock_quantity($min_stock);

				// check if product is out of stock
				if ($min_stock == 0) {

					// check if back orders are allowed
					$back_order = $parent_product->backorders_allowed();

					// Update stock status according to back order availability
					if ($back_order) {
						update_post_meta($parent_product->get_id(), '_stock_status', 'onbackorder');
					} else {
						update_post_meta($parent_product->get_id(), '_stock_status', 'outofstock');
					}
				} else {
					update_post_meta($parent_product->get_id(), '_stock_status', 'instock');
				}
				$parent_product->save();
			}
		}
	}
}
