<?php
include "./ComplexProduct.php";
include "./Product.php";
include "./ProductStock.php";
include "./GetStockComplexProject.php";

## test 1
$testTtitel = "test 1 A, B => expect 100";
$product1 = new ComplexProductComponent(10, "zeep1", 1);
$product1Stock = new ProductStock($product1, 100);

$product2 = new ComplexProductComponent(11, "zeep2", 1);
$product2Stock = new ProductStock($product2, 500);

$complexProduct = new ComplexProduct([$product1, $product2]);

$expectedStockNumber = 100;
$numberInStock = CalculateStock($complexProduct, [$product1Stock, $product2Stock]);

echo $testTtitel . "\n";
echo "NumberInStock: " . $numberInStock . " expected: " . $expectedStockNumber . "\n";
assert($numberInStock == $expectedStockNumber);

## test 2
$testTtitel = "test 2 2A, B => expect 50";
$product1 = new ComplexProductComponent(10, "zeep1", 2);
$product1Stock = new ProductStock($product1, 100);

$product2 = new ComplexProductComponent(11, "zeep2", 1);
$product2Stock = new ProductStock($product2, 500);

$complexProduct = new ComplexProduct([$product1, $product2]);

$expectedStockNumber = 50;
$numberInStock = CalculateStock($complexProduct, [$product1Stock, $product2Stock]);

echo $testTtitel . "\n";
echo "NumberInStock: " . $numberInStock . " expected: " . $expectedStockNumber . "\n";
assert($numberInStock == $expectedStockNumber);

## test 3
$testTtitel = "test 3 3A, B => expect 33";
$product1 = new ComplexProductComponent(10, "zeep1", 3);
$product1Stock = new ProductStock($product1, 100);

$product2 = new ComplexProductComponent(11, "zeep2", 1);
$product2Stock = new ProductStock($product2, 500);

$complexProduct = new ComplexProduct([$product1, $product2]);

$expectedStockNumber = 33;
$numberInStock = CalculateStock($complexProduct, [$product1Stock, $product2Stock]);

echo $testTtitel . "\n";
echo "NumberInStock: " . $numberInStock . " expected: " . $expectedStockNumber . "\n";
assert($numberInStock == $expectedStockNumber);

## test 4
$testTtitel = "test 4 3A, B, 2C => expect 5";
$product1 = new ComplexProductComponent(10, "zeep1", 3);
$product1Stock = new ProductStock($product1, 100);

$product2 = new ComplexProductComponent(11, "zeep2", 1);
$product2Stock = new ProductStock($product2, 500);

$product3 = new ComplexProductComponent(12, "zeep3", 2);
$product3Stock = new ProductStock($product3, 10);

$complexProduct = new ComplexProduct([$product1, $product2, $product3]);

$expectedStockNumber = 5;
$numberInStock = CalculateStock($complexProduct, [$product1Stock, $product2Stock, $product3Stock]);

echo $testTtitel . "\n";
echo "NumberInStock: " . $numberInStock . " expected: " . $expectedStockNumber . "\n";
assert($numberInStock == $expectedStockNumber);

## test 5
$testTtitel = "test 5 3A, B, 2C => expect 0";
$product1 = new ComplexProductComponent(10, "zeep1", 3);
$product1Stock = new ProductStock($product1, 100);

$product2 = new ComplexProductComponent(11, "zeep2", 1);
$product2Stock = new ProductStock($product2, 500);

$product3 = new ComplexProductComponent(12, "zeep3", 2);
$product3Stock = new ProductStock($product3, -1);

$complexProduct = new ComplexProduct([$product1, $product2, $product3]);

$expectedStockNumber = 0;
$numberInStock = CalculateStock($complexProduct, [$product1Stock, $product2Stock, $product3Stock]);

echo $testTtitel . "\n";
echo "NumberInStock: " . $numberInStock . " expected: " . $expectedStockNumber . "\n";
assert($numberInStock == $expectedStockNumber);

## test 5
$testTtitel = "test 5 3A, B, 2C => expect 0";
$product1 = new ComplexProductComponent(10, "zeep1", 3);
$product1Stock = new ProductStock($product1, 100);

$product2 = new ComplexProductComponent(11, "zeep2", 1);
$product2Stock = new ProductStock($product2, 500);

$product3 = new ComplexProductComponent(12, "zeep3", 2);
$product3Stock = new ProductStock($product3, 0);

$complexProduct = new ComplexProduct([$product1, $product2, $product3]);

$expectedStockNumber = 0;
$numberInStock = CalculateStock($complexProduct, [$product1Stock, $product2Stock, $product3Stock]);

echo $testTtitel . "\n";
echo "NumberInStock: " . $numberInStock . " expected: " . $expectedStockNumber . "\n";
assert($numberInStock == $expectedStockNumber);
